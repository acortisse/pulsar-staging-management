# Introduction

As Proxyclick developers, when developing locally, we create topics on our "Staging Pulsar" that are only relevant to us.

Here is an example of topic you might be creating in staging:

`/ws/v2/consumer/persistent/$PULSAR_USERNAME/messaging/mail/sender`

The thing is, since you're in a development phase, you might introduce bugs that cause messages to be inconsistent, causing your services not to be able to process those messages.

Ultimately, you might want to get rid of all those incoherent messages.

Unfortunately, there is currently no suitable web-based UI to do so easily.

## pulsar-admin

There is a solution though: [pulsar-admin](https://pulsar.apache.org/docs/v2.0.1-incubating/reference/CliTools/#pulsar-admin-bxvzl6).

With `pulsar-admin`, you'll be able to delete your topics in no time.

### Installation

Do the following:

- `cp ./conf/.env.example ./conf/.env`
- Replace all the env variables in [.env](.conf/.env.example)
- `sh init.sh`

### Run

- `sh run.sh`

#### Is it working?

Once you've executed [run.sh](./run.sh), do the following:
`sh list-namespaces.sh`
if you're able to retrieve your namespaces, it means you're all set and you can play with the terminal.

# Interesting commands

## Get namespaces

[Doc](https://pulsar.apache.org/docs/en/pulsar-admin/#list-3)

`pulsar-admin namespaces list $PULSAR_USERNAME`

## Get namespace's topics

[Doc](https://pulsar.apache.org/docs/en/pulsar-admin/#list-5)

`pulsar-admin topics list [NAMESPACE]`

## Delete topic

[Doc](https://pulsar.apache.org/docs/en/pulsar-admin/#delete-4)

`pulsar-admin topics delete -f [TOPIC_NAME]`

## Get topic's subscriptions

[Doc](https://pulsar.apache.org/docs/en/pulsar-admin/#subscriptions)

`pulsar-admin topics subscriptions [TOPIC_NAME]`

## Delete a subscription (and all its messages)

[Doc](https://pulsar.apache.org/docs/en/pulsar-admin/#unsubscribe-1)

`pulsar-admin topics unsubscribe [TOPIC_NAME] --subscription [SUBSCRIPTION_NAME] --force`

## Duplicate subscription

[Doc](https://pulsar.apache.org/docs/en/pulsar-admin/#create-subscription)

`pulsar-admin topics create-subscription --messageId earliest -s [TOPIC_NAME_TO_DRAW_FROM] [NEW_TOPIC_NAME]`

### Example

`pulsar-admin topics create-subscription --messageId earliest -s persistent://$PULSAR_USERNAME/messaging/mail persistent://$PULSAR_USERNAME/messaging/mail_copy`
