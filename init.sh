#!/bin/bash


set -a # automatically export all variables
source ./conf/.env
set +a

echo $PULSAR_KEY

mkdir -p ./conf/pulsar-conf

cp $PULSAR_KEY ./conf/pulsar-conf
cp $PULSAR_CA_CERT ./conf/pulsar-conf
cp $PULSAR_CERT ./conf/pulsar-conf

docker build --build-arg PULSAR_USERNAME=$PULSAR_USERNAME --tag pulsar-admin .
