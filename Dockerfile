FROM ubuntu:18.04

WORKDIR /app

RUN apt-get update
RUN apt-get install -y  
RUN apt-get install wget -y

RUN wget https://archive.apache.org/dist/pulsar/pulsar-2.8.1/apache-pulsar-2.8.1-bin.tar.gz
RUN tar xvzf apache-pulsar-2.8.1-bin.tar.gz
RUN rm -rf apache-pulsar-2.8.1-bin.tar.gz

RUN apt-get install openjdk-8-jdk -y

ARG PULSAR_USERNAME
ENV pulsar_admin_path="/app/apache-pulsar-2.8.1/bin/pulsar-admin"
ENV PULSAR_USERNAME=$PULSAR_USERNAME

RUN echo 'alias pulsar-admin="${pulsar_admin_path}"' >> ~/.bashrc

COPY ./conf/client.conf /app/apache-pulsar-2.8.1/conf

COPY ./conf/pulsar-conf /app/pulsar-conf

COPY scripts scripts

CMD ["/bin/bash"]